// Copyright Kevin Hudson 2020.


#include "OpenGate.h"
#include "Components/AudioComponent.h"
#include "Components/PrimitiveComponent.h"
#include "Engine/World.h"
#include "GameFramework/Actor.h"
#include "GameFramework/PlayerController.h"

#define OUT

// Sets default values for this component's properties
UOpenGate::UOpenGate()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	OpenGateAudioComponent = CreateDefaultSubobject<UAudioComponent>(TEXT("OpenGateAudioComponent"));
    CloseGateAudioComponent = CreateDefaultSubobject<UAudioComponent>(TEXT("CloseGateAudioComponent"));
}


// Called when the game starts
void UOpenGate::BeginPlay()
{
	Super::BeginPlay();

	InitialZ = GetOwner()->GetActorTransform().GetLocation().Z;
	CurrentZ = InitialZ;
	OpenHeight += InitialZ;

	FindGateTrigger();
	
}



void UOpenGate::FindGateTrigger()
{
	if (!GateTrigger)
	{
		UE_LOG(LogTemp, Warning, TEXT("%s has OpenGate Component without a GateTrigger! "), *GetOwner() -> GetName());
	}
}





// Called every frame
void UOpenGate::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	CurrentTime = GetWorld() ->GetTimeSeconds();

	if (TotalMassOfActors() > MassToOpenGates)
	{
		OpenGate(DeltaTime);
		GateLastOpened = GetWorld() -> GetTimeSeconds();
	}
	else
	{
		if (CurrentTime - GateLastOpened > GateCloseDelay )
		{
			CloseGate(DeltaTime);
		}
		
	}
}


void UOpenGate::OpenGate(float DeltaTime)
{
	CurrentZ = FMath::FInterpTo(
		CurrentZ,
		OpenHeight,
		DeltaTime,
		GateOpenSpeed
		);
		
	FVector GateZ = GetOwner() -> GetActorLocation();
	GateZ.Z = CurrentZ;
	GetOwner()->SetActorLocation(GateZ);

	CloseGateSound = false;
	if (!OpenGateAudioComponent) {return;}
	if (!OpenGateSound)
	{
		
		OpenGateAudioComponent ->Play();
		OpenGateSound = true;
	}

}

void UOpenGate::CloseGate(float DeltaTime)
{
	CurrentZ = FMath::FInterpTo(
		CurrentZ,
		InitialZ,
		DeltaTime,
		GateClosedSpeed
		);
		
	FVector GateZ = GetOwner() -> GetActorLocation();
	GateZ.Z = CurrentZ;
	GetOwner()->SetActorLocation(GateZ);

	OpenGateSound = false;
	if (!CloseGateAudioComponent) {return;}
	if (!CloseGateSound)
	{
		CloseGateAudioComponent ->Play();
		CloseGateSound = true;
	}

}

float UOpenGate::TotalMassOfActors() const
{
	float TotalMass = 0.f;


	// Find all Overlapping actors 
	TArray<AActor*> OverlappingActors;
	if(!GateTrigger) {return TotalMass;}
	GateTrigger -> GetOverlappingActors(OUT OverlappingActors);

	// Add up their masses. 
	for (AActor* Actor : OverlappingActors)
	{
		TotalMass += Actor ->FindComponentByClass<UPrimitiveComponent>() -> GetMass();
	}
	
	return TotalMass;
}