// Copyright Kevin Hudson 2020.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Engine/TriggerVolume.h"
#include "OpenGate.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class BUILDING_ESCAPE_API UOpenGate : public UActorComponent
{
	GENERATED_BODY()



protected:
// Called when the game starts
virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;
	void OpenGate(float DeltaTime);
	void CloseGate(float DeltaTime);
	float TotalMassOfActors() const; 
	void FindGateTrigger();

	
	//Tracks sound being played
	bool OpenGateSound = false;
	bool CloseGateSound = true;

public:	
	// Sets default values for this component's properties
	UOpenGate();



private: 
// Private declarations 
float InitialZ;
float CurrentZ;

UPROPERTY(EditAnywhere)
float OpenHeight = 600.f;

UPROPERTY(EditAnywhere)
ATriggerVolume* GateTrigger = nullptr;

UPROPERTY(EditAnywhere)
float GateCloseDelay = 2.f;

UPROPERTY(EditAnywhere)
float GateClosedSpeed = 2.f;

UPROPERTY(EditAnywhere)
float GateOpenSpeed = 2.f;

UPROPERTY(EditAnywhere)
float MassToOpenGates = 50.f;

UPROPERTY(EditAnywhere)
UAudioComponent* OpenGateAudioComponent;

UPROPERTY(EditAnywhere)
UAudioComponent* CloseGateAudioComponent;

float CurrentTime = 0.f;
float GateLastOpened = 0.f;

		
};
